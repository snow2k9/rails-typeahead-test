Rails.application.routes.draw do
  
  resources :authors, only: [:index]
  root 'app#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
