class Author < ApplicationRecord
  include Filterable

  scope :q, -> (name) { where(Author.arel_table[:name].matches("%#{name}%")) }
end
