class AuthorsController < ApplicationController

  # GET /authors
  # GET /authors.json
  def index
    @authors = Author.filter(params.slice(:q))
  end
  
end
