// instantiate the bloodhound suggestion engine
var numbers = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('num'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  local: [
    { num: 'one' },
    { num: 'two' },
    { num: 'three' },
    { num: 'four' },
    { num: 'five' },
    { num: 'six' },
    { num: 'seven' },
    { num: 'eight' },
    { num: 'nine' },
    { num: 'ten' }
  ]
});

var authors = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: '/authors.json',
  remote: {
    url: '/authors.json?q=%QUERY',
    wildcard: '%QUERY'
  }
});

function inititate_numbers() {
  // initialize the bloodhound suggestion engine
  numbers.initialize();
  // instantiate the typeahead UI
  $('#search-numbers').typeahead(null, {
    displayKey: 'num',
    source: numbers.ttAdapter()
  });
}

function inititate_authors() {
  // initialize the bloodhound suggestion engine
  authors.initialize();
  // instantiate the typeahead UI
  $('#search-authors').typeahead(null, {
    displayKey: 'name',
    source: authors.ttAdapter()
  });
}

$(document).on('turbolinks:load', function () {
  inititate_numbers();
  inititate_authors();
})
