# Rails Typeahead Demo

## Usage
```bash
git clone https://gitlab.com/snow2k9/rails-typeahead-test
cd rails-typeahead-test
bundle install
rails db:setup
rails db:seed
rails server
```

Browse to `http://localhost:3000` and take a look

## About

I wanted to play with [typeahead](http://twitter.github.io/typeahead.js) and created a small rails app

Thanks to Justin Weiss for the [Filterable](https://www.justinweiss.com/articles/search-and-filter-rails-models-without-bloating-your-controller/) Module I am using for my remote query

## License

see [License](LICENSE)
